// 3-rd part libs
import * as React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// components
import Content from './components/Content/Content';

// styles
import './reset.css';

// reducers
import rootReducer from './store';

const store = createStore(rootReducer, applyMiddleware(thunk));

render(
    <Provider store={store}>
        <Router>
            <Content />
        </Router>
    </Provider>,
    document.getElementById('root'),
);
