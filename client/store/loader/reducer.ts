import { Loader } from './types/actions';
import ILoaderState from './types/store';
import { LoaderType } from './types/loaderType';

const initialState: ILoaderState = {};

type LoaderAction = {
    type: string;
    payload: keyof typeof LoaderType;
};

const reducer = (state = initialState, { type, payload: loaderType }: LoaderAction) => {
    switch (type) {
        case Loader.SHOW_LOADER:
            return { [loaderType]: true };
        case Loader.HIDE_LOADER:
            return { [loaderType]: false };
        default:
            return state;
    }
};

export default reducer;
