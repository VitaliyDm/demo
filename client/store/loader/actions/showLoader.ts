import { Loader } from '../types/actions';
import { LoaderType } from '../types/loaderType';

const showLoader = (loaderType: keyof typeof LoaderType = LoaderType.loadingPage) => {
    return {
        type: Loader.SHOW_LOADER,
        payload: loaderType,
    };
};

export default showLoader;
