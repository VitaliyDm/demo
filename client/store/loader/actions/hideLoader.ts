import { Loader } from '../types/actions';
import { LoaderType } from '../types/loaderType';

const hideLoader = (loaderType: keyof typeof LoaderType = LoaderType.loadingPage) => ({
    type: Loader.HIDE_LOADER,
    payload: loaderType,
});

export default hideLoader;
