import { Error } from './types/actions';

type ErrorAction = {
    type: string;
    payload: string;
};

const reducer = (state = null, { type, payload }: ErrorAction) => {
    switch (type) {
        case Error.ADD_ERROR:
            return payload;
        case Error.REMOVE_ERROR:
            return null;
        default:
            return state;
    }
};

export default reducer;
