interface Error {
    code: string;
    text: string;
}

export default Error;
