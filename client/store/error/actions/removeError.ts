import { Error } from '../types/actions';

const removeError = () => ({
    type: Error.REMOVE_ERROR,
});

export default removeError;
