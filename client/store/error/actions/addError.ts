import { Error } from '../types/actions';

const addError = (message: string) => ({
    type: Error.ADD_ERROR,
    payload: message,
});

export default addError;
