import { combineReducers } from 'redux';
import loader from './loader/reducer';
import cards from './cards/reducer';
import error from './error/reducer';
import quest from './quest/reducer';

export default combineReducers({
    loader: loader,
    cards: cards,
    error: error,
    quest: quest
});
