// types
import Quest from '../types/Quest';
import QuestActions from '../types/actions';

// actions
import requestAction from '../../requestAction';
import { Loader } from '../../loader/types/actions';

const getQuestPage = (questName: string, pageIndex: string) => dispatch => dispatch(
    requestAction({
        method: `/api/quest?pageIndex=${pageIndex}&questName=${questName}`
    })
)
    .then((res: Quest) => {
        dispatch({
            payload: res,
            type: QuestActions.LOAD_PAGE
        });
        dispatch({
            type: Loader.HIDE_LOADER
        });
    });

export default getQuestPage;
