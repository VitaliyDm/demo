import Quest from './Quest';

interface IQuestStoreState {
    quest: Quest
}

export default IQuestStoreState;
