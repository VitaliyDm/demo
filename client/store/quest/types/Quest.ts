interface Controls {
    title: string;
    link: string;
}

interface Achievement {
    image: string;
    title: string;
}

interface Quest {
    questName: string;
    image: string;
    content: string;
    contentPlace: string;
    achievements: Achievement[];
    controls: Controls[];
}

export default Quest;
export {
    Controls,
    Achievement
};
