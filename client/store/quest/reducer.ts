import QuestActions from './types/actions';
import IQuestStoreState from "./types/store";

type QuestAction = {
    type: string;
    payload: any;
};

const initialState: IQuestStoreState = {
    quest: {
        questName: '',
        image: '',
        content: '',
        contentPlace: '',
        achievements: [],
        controls: [],
    }
};

const reducer = (state = initialState, { type, payload }: QuestAction) => {
    switch (type) {
        case QuestActions.LOAD_PAGE:
            return { ...state, quest: payload };
        default:
            return state;
    }
};

export default reducer;
