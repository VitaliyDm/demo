enum Cards {
    CARDS_LOADED = 'CARDS_LOADED',
    FILTERS_LOADED = 'FILTERS_LOADED'
}

export default Cards;
