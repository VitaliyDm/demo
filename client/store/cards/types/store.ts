import Card from './Card';

interface ICardStoreState {
    cards: Card[];
    filter: string;
}

export default ICardStoreState;
