interface Tag {
    title: string;
    link: string;
}

interface Card {
    imageAddress: string;
    link: string;
    title: string;
    description: string;
    tags: Tag[];
}

export default Card;
export {
    Tag
};
