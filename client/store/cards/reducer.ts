import Cards from './types/actions';
import ICardStoreState from './types/store';

type CardAction = {
    type: string;
    payload: any;
};

const initialState: ICardStoreState = {
    cards: [],
    filter: ''
};

const reducer = (state = initialState, { type, payload }: CardAction) => {
    switch (type) {
        case Cards.CARDS_LOADED:
            return { ...state, cards: payload.cards };
        case Cards.FILTERS_LOADED:
            return {
                ...state,
                cards: payload.cards,
                filter: payload.filterTag
            };
        default:
            return state;
    }
};

export default reducer;
