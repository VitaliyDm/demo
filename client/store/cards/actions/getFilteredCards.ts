// types
import Cards from '../types/actions';
import Card from '../types/Card';
import { Loader } from '../../loader/types/actions';

// actions
import requestAction from '../../requestAction';

const getFilteredCards = (filter: string) => dispatch => dispatch(
    requestAction({
        method: `/api/cards?filterTag=${filter}`
    })
)
    .then((res: Card) => {
        dispatch({
            payload: res,
            type: Cards.FILTERS_LOADED
        });
        dispatch({
            type: Loader.HIDE_LOADER
        });
    });

export default getFilteredCards;
