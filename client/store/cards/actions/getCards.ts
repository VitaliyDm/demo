// types
import Cards from '../types/actions';
import Card from '../types/Card';
import { Loader } from '../../loader/types/actions';


import requestAction from '../../requestAction';

const getCards = () => dispatch => dispatch(
    requestAction({
        method: '/api/cards'
    })
)
    .then((res: Card) => {
        dispatch({
            payload: res,
            type: Cards.CARDS_LOADED
        });
        dispatch({
            type: Loader.HIDE_LOADER
        });
    });

export default getCards;
