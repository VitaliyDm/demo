// services
import requestService from '../services/request';

// actions
import addError from './error/actions/addError';
import removeError from './error/actions/removeError';

const requestAction = ({
    method, payload, errorType, ...rest
}: any) => dispatch => requestService({
    method,
    payload,
    ...rest,
})
    .then(res => {
        if (!res.ok) {

            return res.json().then(res => Promise.reject(res));
        }
        return res.json()
    })
    .then(result => {
        dispatch(removeError());

        return result;
    })
    .catch(error => {
        dispatch(addError(error || 'There was an reducer'));

        return Promise.reject(error);
    });

export default requestAction;
