// 3-rd party libs
import * as React from 'react';

// styles
import style from './Loader.css';

const Loader = () => (
    <div className={style.ldsRing}>
        <div />
        <div />
        <div />
        <div> /</div>
    </div>
);

export default Loader;
