// 3-rd party libs
import * as React from 'react';
import { Link } from 'react-router-dom';

// components
import Tag from '../Tag/Tag';

// constants
import envParams from '../../constants/envParams';

// types
import Card from '../../store/cards/types/Card';

// styles
import style from './QuestCard.css';

const QuestCard = ({
    description, imageAddress, link, tags, title
}: Card) => (
    <div className={style.questCardWrapper}>
        <img src={`${envParams.staticBasePath}${imageAddress}`} className={style.questCardImage} />
        <div className={style.questCardContent}>
            <Link to={`/quest/${link}`} className={style.questCardTitle}>{title}</Link>
            <p className={style.questCardDescription}>{description}</p>
            <div className={style.questCardTagWrapper}>
                { tags.map(tag => <Tag title={tag.title} link={tag.link} key={tag.link} />)}
            </div>
        </div>
    </div>
);

export default QuestCard;
