// 3-rd party libs
import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import Error from '../../store/error/types/Error';

// style
import style from './Error.css';

// actions
import removeError from '../../store/error/actions/removeError';

const mapStateToProps = (props: Error) => ({
    ...props
});

const mapDispatchToProps = {
    removeError
};

const storeEnhancer = connect(mapStateToProps, mapDispatchToProps);

type ErrorProps = Error & ConnectedProps<typeof storeEnhancer>;

const ErrorPage = ({ text, code, removeError }: ErrorProps) => {
    React.useEffect(() => removeError, []);

    return (
        <div className={style.errorWrapper}>
            <h1 className={style.errorCode}>{code}</h1>
            <h3 className={style.errorText}>{text}</h3>
        </div>);
};

export default storeEnhancer(ErrorPage);
