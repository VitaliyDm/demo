// 3-rd party libs
import * as React from 'react';
import {connect, ConnectedProps} from 'react-redux';
import { withRouter } from 'react-router';

// types
import { IGlobalState } from '../../types/base/store';
import { LoaderType } from '../../store/loader/types/loaderType';
import { IRouterParams } from '../../types/inner/match';

// components
import Controls from './Controls';
import Achievement from './Achievement';
import QuestContent from './QuestContent';
import Loader from '../Loader/Loader';

// actions
import getQuestPage from '../../store/quest/actions/getQuestPage';
import showLoader from '../../store/loader/actions/showLoader';

// styles
import style from './QuestPage.css';

const mapStateToProps = ({ quest, loader }: IGlobalState) => ({
    loader: loader[LoaderType.loadingPage],
    quest: quest.quest
});

const mapDispatchToProps = {
    getQuestPage,
    showLoader
};

const storeEnhancer = connect(
    mapStateToProps,
    mapDispatchToProps
);

type QuestPageProps = ConnectedProps<typeof storeEnhancer> & IRouterParams;

const QuestPage = ({
    match, location, loader, quest, getQuestPage, showLoader
}: QuestPageProps) => {
    React.useEffect(() => {
        getQuestPage(match.params.questName, match.params.page);
        showLoader();
    }, [location.pathname]);

    if (loader || !quest) {
        return <Loader />;
    }

    return (<>
        <QuestContent
            image={quest.image}
            content={quest.content}
        />
        {
            quest.achievements.length > 0
                && quest.achievements.map(achievment => <Achievement
                    image={achievment.image}
                    title={achievment.title}
                    key={achievment.title}
                />)
        }
        {
            quest.controls.length > 0
                && <div className={style.controlsWrapper}>
                    {
                        quest.controls.map(control => <Controls
                            title={control.title}
                            link={control.link}
                            key={control.link}
                        />)
                    }
                </div>
        }
    </>);
};

export default withRouter(storeEnhancer(QuestPage));
