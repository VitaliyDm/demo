// 3-rd party libs
import * as React from 'react';
import classNames from 'classnames';

// types
import Quest from '../../store/quest/types/Quest';

// constants
import envParams from '../../constants/envParams';

// styles
import style from './QuestPage.css';

const cn = classNames.bind(style);

const QuestContent = (props: Pick<Quest, 'image' | 'content' | 'contentPlace'>) => (
    <div
        className={style.questContentWrapper}
        style={{
            background: `url('${envParams.staticBasePath}${props.image}')`,
        }}
    >
        <p className={cn(props.contentPlace, style.questContentText)}>{props.content}</p>
    </div>
);

export default QuestContent;
