// 3-rd party libs
import * as React from 'react';
import { Link } from 'react-router-dom';

// types
import { Controls } from '../../store/quest/types/Quest';

// styles
import style from './QuestPage.css';

const ControlsComponent = (props: Controls) => (
    <Link to={props.link}>
        <div className={style.controlButton}>
            {props.title}
        </div>
    </Link>
);

export default ControlsComponent;
