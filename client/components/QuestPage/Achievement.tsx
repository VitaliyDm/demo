// 3-rd party libs
import * as React from 'react';

// types
import { Achievement } from '../../store/quest/types/Quest';

// constants
import envParams from '../../constants/envParams';

// styles
import style from './QuestPage.css';

const Achievement = (props: Achievement) => (
    <div className={style.achievementWrapper}>
        <img src={`${envParams.staticBasePath}${props.image}`} className={style.achievementImage} />
        <div className={style.achievementContent}>
            <h3 className={style.achievementHeaderText}>Достижение получено</h3>
            <h2 className={style.achievementName}>{props.title}</h2>
        </div>
    </div>
);

export default Achievement;
