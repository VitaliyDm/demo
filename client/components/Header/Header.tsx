// 3-rd part libs
import * as React from 'react';
import { Link } from 'react-router-dom';

// images
import logo from '../../assets/images/logo.png';

// styles
import style from './Header.css';

const Header = () => (
    <header className={style.headerWrapper}>
        <Link to="/">
            <div className={style.headerLink}>
                <img src={logo} alt="logo" className={style.headerLogo} />
                <h1>
                    <span className={style.logoPinkTitle}>Telltail</span>
                    <span className={style.logoTitle}>Games</span>
                </h1>
            </div>
        </Link>
    </header>
);

export default Header;
