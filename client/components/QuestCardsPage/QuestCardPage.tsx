// 3-rd party libs
import * as React from 'react';
import {connect, ConnectedProps} from 'react-redux';
import { withRouter } from 'react-router';

// component
import QuestCard from '../QuestCard/QuestCard';
import Loader from '../Loader/Loader';

// types
import { IGlobalState } from '../../types/base/store';
import { IRouterParams } from '../../types/inner/match';
import { LoaderType } from '../../store/loader/types/loaderType';

// actions
import getCards from '../../store/cards/actions/getCards';
import getFilteredCards from '../../store/cards/actions/getFilteredCards';

// utils
import showLoader from '../../store/loader/actions/showLoader';

// styles
import style from './QuestCardPage.css';

const mapStateToProps = ({ cards, loader }: IGlobalState, props) => ({
    ...props,
    cards: cards.cards,
    filter: cards.filter,
    loader: loader[LoaderType.loadingPage]
});

const mapDispatchToProps = {
    getCards,
    getFilteredCards,
    showLoader
};

const storeEnhancer = connect(
    mapStateToProps,
    mapDispatchToProps
);

type QuestCardPageProps = IRouterParams & ConnectedProps<typeof storeEnhancer>;

const QuestCardPage = (props: QuestCardPageProps) => {
    React.useEffect(() => {
        if (props.match.params.filter) {
            props.getFilteredCards(props.match.params.filter);
        } else {
            props.getCards();
        }
        props.showLoader();
    }, [props.match.params.filter]);

    if (props.loader) {
        return <Loader />;
    }

    return (
        <>
            {
                props.match.params.filter
                && <span className={style.filterText}>{props.filter}</span>
            }
            {props.cards.map(item => <QuestCard
                imageAddress={item.imageAddress}
                link={item.link}
                title={item.title}
                description={item.description}
                tags={item.tags}
                key={item.link}
            />)}
        </>
    );
};

export default withRouter(storeEnhancer(QuestCardPage));
