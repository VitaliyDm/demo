// 3-rd party libs
import * as React from 'react';
import { Link } from 'react-router-dom';

// types
import { Tag } from '../../store/cards/types/Card';

// styles
import style from './Tag.css';

const Tag = ({ link, title }: Tag) => (
    <Link to={ `/cards/${link}` }>
        <div className={style.tagContainer}>
            {title}
        </div>
    </Link>
);

export default Tag;
