// 3rd party libs
import * as React from 'react';
import {
    Redirect, Route, Switch, withRouter
} from 'react-router';
import { connect } from 'react-redux';

// components
import Header from '../Header/Header';
import QuestCardPage from '../QuestCardsPage/QuestCardPage';
import QuestPage from '../QuestPage/QuestPage';
import ErrorPage from '../Error/Error';

// types
import { IGlobalState } from '../../types/base/store';

// styles
import style from './Content.css';

const mapStateToProps = ({ error }: IGlobalState) => ({
    error
});

const storeEnhancer = connect(
    mapStateToProps
);

const Content = ({ error }) => (
    <>
        <Header/>
        <div className={style.wrapper}>
            <Switch>
                {
                    error
                    && <>
                        <Route path='/err'>
                            <ErrorPage code={error.code} text={error.text}/>
                        </Route>
                        <Redirect to='/err'/>
                    </>
                }
                <Route
                    exact
                    path='/cards/:filter'
                    component={QuestCardPage}
                />
                <Route
                    exact
                    path='/cards'
                >
                    <QuestCardPage/>
                </Route>
                <Route
                    path='/quest/:questName/:page'
                >
                    <QuestPage/>
                </Route>
                <Redirect to='/cards' from='/'/>
            </Switch>
        </div>
    </>
);

export default withRouter(storeEnhancer(Content));
