import { RouteComponentProps } from 'react-router';

interface IMatchParams {
    questName: string;
    page: string;
    filter: string;
}

type IRouterParams = RouteComponentProps<IMatchParams>;

export {
    IMatchParams,
    IRouterParams
};
