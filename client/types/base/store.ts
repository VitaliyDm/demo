import ILoaderState from '../../store/loader/types/store';
import IErrorState from '../../store/error/types/store';
import ICardStoreState from '../../store/cards/types/store';
import IQuestStoreState from "../../store/quest/types/store";

interface IGlobalState {
    loader: ILoaderState;
    cards: ICardStoreState;
    error: IErrorState;
    quest: IQuestStoreState;
}

export { IGlobalState };
