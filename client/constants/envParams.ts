const envParams = {
    staticBasePath: process.env.STATIC_BASE_PATH
};

export default envParams;
