'use strict';

module.exports = {
    debug: true,
    port: 3000,
    STATIC_BASE_PATH: 'http://localhost:3000',
    API_HOST: 'https://vast-oasis-18259.herokuapp.com',
    databaseAddress: 'postgres://oheqchph:iyjlbcmZLuCjdEjbQ1yeioVK5l7PHx87@arjuna.db.elephantsql.com:5432/oheqchph',
    dataCacheTimeout: 3600, // in seconds
    allowedOrigins: [
        'https://vast-oasis-18259.herokuapp.com',
        'http://localhost:4200'
    ]
};
