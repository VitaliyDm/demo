const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const dist = path.resolve(__dirname, '../dist/front');
const entry = path.resolve(__dirname, '../client/index.tsx');
const index = path.resolve(__dirname, '../client/index.html');
const postcss = path.resolve(__dirname, './styles')

const isProduction = process.env.NODE_ENV === 'production';
const API_HOST = process.env.API_HOST || 'https://vast-oasis-18259.herokuapp.com';
const STATIC_BASE_PATH = process.env.STATIC_BASE_PATH || 'https://vitaliydm.github.io/'

module.exports = {
    entry,
    output: {
        filename: '[name].bundle.js',
        path: dist,
        publicPath: '/',
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'spheridian',
            template: index,
        }),
        new webpack.EnvironmentPlugin({
            SERVER: false,
            API_HOST,
            STATIC_BASE_PATH
        }),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: isProduction
                                    ? '[hash:base64:5]'
                                    : '[name]__[local]_[hash:base64:5]',
                            },
                            importLoaders: 1,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            config: {
                                path: postcss
                            }
                        }
                    },
                ],
            },
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                            experimentalWatchApi: true,
                        },
                    },
                ],
                exclude: /node_modules/,
            },
            {
                test: /\.(svg|png|gif|jpg|webp|eot|woff|ttf)$/,
                loader: 'file-loader',
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
};
