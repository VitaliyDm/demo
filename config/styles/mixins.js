const sizes = {
    h1: {
        'font-size': '48px',
        'line-height': '45px'
    },
    h2: {
        'font-size': '42px',
        'line-height': '42px'
    },
    h3: {
        'font-size': '30px',
        'line-height': '30px'
    },
    p: {
        'font-size': '18px',
        'line-height': '28px'
    }
}

const fonts = {
    roboto: {
        'font-family': 'Roboto, sans-serif'
    },
    robotoLight: {
        'font-family': '"Roboto script=all rev=1", "Adobe Blank", sans-serif',
        'font-weight': 100
    },
    gabriela: {
        'font-family': 'Gabriela, serif'
    }
}

const mixins = {
    text: function (mixin, params) {
        const [font, size] = params.split(' ')
        return {
            '&': {
                ...fonts[font],
                ...sizes[size]
            }
        }
    }
}

module.exports = mixins;
