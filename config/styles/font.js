const fontFallback = 'EllipsisFallback, Roboto, Helvetica, Arial, sans-serif';
const fallbackPfhighway = 'BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, sans-serif';

const result = {
    '--font-fallback': fontFallback,
    '--font-default': `haas, pragmatica, ${fontFallback}`,
    '--font-pfhighway': `pfhighway, ${fallbackPfhighway}`
};

module.exports = {
    customProperties: result
}
