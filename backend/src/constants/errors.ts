const errors = {
    notFound: {
        code: 404,
        text: 'Такой страницы не существует 😬'
    },
    internalError: {
        code: 500,
        text: 'Ошибка сервера 👿'
    }
};

export default errors;
