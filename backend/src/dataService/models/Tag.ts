import {
    Column,
    CreatedAt,
    ForeignKey,
    Model,
    PrimaryKey,
    Table,
    Unique,
    UpdatedAt
} from 'sequelize-typescript';
import QuestCard from './QuestCard';

@Table({
    schema: 'game',
    tableName: 'tag'
})
class Tag extends Model<Tag> {
    @PrimaryKey
    @Unique
    @Column
    public id!: number;

    @Column
    public title!: string;

    @Column
    public link!: string;

    @ForeignKey(() => QuestCard)
    @Column
    public questCardId!: number;

    @CreatedAt
    @Column
    public readonly createdAt!: Date;

    @UpdatedAt
    @Column
    public readonly updatedAt!: Date;
}

export default Tag;
