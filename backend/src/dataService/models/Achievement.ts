// 3-rd party libs
import {
    Model,
    Table,
    PrimaryKey,
    Unique,
    ForeignKey,
    CreatedAt,
    UpdatedAt,
    Column
} from 'sequelize-typescript';
import QuestPage from './QuestPage';

@Table({
    schema: 'game',
    tableName: 'quest_achievements'
})
class Achievement extends Model {
    @Unique
    @PrimaryKey
    @Column
    public id!: number;

    @Column
    public title!: string;

    @Column
    public image!: string;

    @ForeignKey(() => QuestPage)
    @Column
    public questPageId!: number;

    @CreatedAt
    @Column
    public readonly createdAt!: Date;

    @UpdatedAt
    @Column
    public readonly updatedAt!: Date;
}

export default Achievement;
