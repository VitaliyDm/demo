import {
    Model,
    Table,
    HasMany,
    Column,
    PrimaryKey,
    Unique,
    CreatedAt,
    UpdatedAt
} from 'sequelize-typescript';

// models
import Control from './Control';
import Achievement from './Achievement';

@Table({
    schema: 'game',
    tableName: 'quest_pages'
})
class QuestPage extends Model {
    @Unique
    @PrimaryKey
    @Column
    public id!: number;

    @Column
    public questName!: string;

    @Column
    public image!: string;

    @Column
    public content!: string;

    @Column
    public contentPlace!: string;

    @CreatedAt
    @Column
    public readonly createdAt!: Date;

    @UpdatedAt
    @Column
    public readonly updatedAt!: Date;

    @Column
    public indexAtQuest!: number;

    @HasMany(() => Control)
    public readonly controls?: Control[];

    @HasMany(() => Achievement)
    public readonly achievements?: Achievement[];
}

export default QuestPage;
