// 3-rd party libs
import {
    Table,
    Model,
    Column,
    PrimaryKey,
    ForeignKey,
    Unique,
    CreatedAt,
    UpdatedAt
} from 'sequelize-typescript';
import QuestPage from './QuestPage';

@Table({
    schema: 'game',
    tableName: 'quest_controls'
})
class Control extends Model {
    @Unique
    @PrimaryKey
    @Column
    public id!: number;

    @Column
    public title!: string;

    @Column
    public link!: string;

    @ForeignKey(() => QuestPage)
    @Column
    public questPageId!: number;

    @CreatedAt
    @Column
    public readonly createdAt!: Date;

    @UpdatedAt
    @Column
    public readonly updatedAt!: Date;
}

export default Control;
