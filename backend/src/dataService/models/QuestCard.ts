import {
    Column,
    CreatedAt,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    UpdatedAt
} from 'sequelize-typescript';
import Tag from './Tag';

@Table({
    schema: 'game',
    tableName: 'quest_card'
})
class QuestCard extends Model<QuestCard> {
    @PrimaryKey
    @Column
    public id!: number;

    @Column
    public title!: string;

    @Column
    public imageAddress!: string;

    @Column
    public description!: string;

    @Column
    public link!: string;

    @CreatedAt
    @Column
    public readonly createdAt!: Date;

    @UpdatedAt
    @Column
    public readonly updatedAt!: Date;

    @HasMany(() => Tag)
    public readonly tags?: Tag[];
}

export default QuestCard;
