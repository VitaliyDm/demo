// 3-rd party libs
import config from 'config';
import { Sequelize } from 'sequelize-typescript';

// models
import QuestCard from './models/QuestCard';
import QuestPage from './models/QuestPage';
import Tag from './models/Tag';
import Achievement from './models/Achievement';
import Control from './models/Control';

// utils
import getCurrentTimeInSeconds from '../utils/getCurrentTimeInSeconds';

// types
import Quest from '../types/Quest';
import { CardsCache, PagesCache } from '../types/Cache';

// simple cache
let cardsCache: CardsCache = {
    data: [] as Quest[],
    lastUpdate: 0
};

const pagesCache: PagesCache = {};

function initDataService() {
    const sequelize = new Sequelize(config.get('databaseAddress'));
    sequelize.addModels([Tag, QuestCard, Achievement, QuestPage, Control]);
}

async function getQuestCards(): Promise<Quest[]> {
    if ((config.get('dataCacheTimeout') as number) > getCurrentTimeInSeconds() - cardsCache.lastUpdate
        && cardsCache.data.length
    ) {
        return cardsCache.data;
    }

    const cards = await QuestCard.findAll({ include: [Tag] });

    const tags = await Promise.all(
        cards.map(async item => item?.tags?.map(tag => ({
            title: tag.title,
            link: tag.link
        })))
    );

    const res = cards.map((item, index) => ({
        tags: tags[index],
        title: item.title,
        imageAddress: item.imageAddress,
        description: item.description,
        link: item.link
    }));

    cardsCache = {
        // @ts-ignore
        data: res,
        lastUpdate: getCurrentTimeInSeconds()
    };

    // @ts-ignore
    return res;
}

async function getQuestPages(questName: string, currentPage: number) {
    if (questName in pagesCache && pagesCache[questName][currentPage]
        && (config.get('dataCacheTimeout') as number) > getCurrentTimeInSeconds() - pagesCache[questName][currentPage].lastUpdate
        && pagesCache[questName][currentPage].data
    ) {
        return pagesCache[questName][currentPage].data;
    }

    const pages = await QuestPage.findAll({
        where: {
            questName
        },
        order: ['indexAtQuest'],
        include: [Achievement, Control]
    });

    if (!pages.length) {
        throw new Error(`quest not found: ${questName}`);
    }
    // @ts-ignore
    const controls = await Promise.all(pages[currentPage]?.controls?.map(control => ({
        title: control.title,
        link: control.link
    })));
    // @ts-ignore
    const achievements = await Promise.all(pages[currentPage]?.achievements?.map(achievement => ({
        title: achievement.title,
        image: achievement.image
    })));

    const res = {
        controls,
        achievements,
        image: pages[currentPage].image,
        content: pages[currentPage].content,
        contentPlace: pages[currentPage].contentPlace
    };

    if (!(questName in pagesCache)) {
        pagesCache[questName] = [];
    }


    pagesCache[questName][currentPage] = {
        // @ts-ignore
        data: res,
        lastUpdate: getCurrentTimeInSeconds()
    };

    return res;
}

export default initDataService;
export {
    getQuestCards,
    getQuestPages
};
