import { ButtonLink } from './PageData';

interface Quest {
    link: string,
    title: string,
    imageAddress: string,
    description: string,
    tags: ButtonLink[]
}

export default Quest;
