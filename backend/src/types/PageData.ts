interface PageData {
    meta?: {
        charset: string;
        description: string;
    };
    title?: string;
    staticBasePath?: string;
}

interface ButtonLink {
    title: string,
    link: string
}

export default PageData;
export {
    ButtonLink
};
