import { ButtonLink } from './PageData';

enum ContentPlace {
    TopLeft = 'top-left',
    TopRight = 'top-right',
    BottomRight = 'bottom-right',
    BottomLeft = 'bottom-left'
}

interface Achievement {
    image: string,
    title: string
}

interface QuestContent {
    image: string,
    content: string,
    contentPlace: ContentPlace,
    controls?: ButtonLink[],
    achievements?: Achievement[]
}

export default QuestContent;
