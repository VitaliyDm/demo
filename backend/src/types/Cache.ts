import QuestContent from './QuestContent';
import Quest from './Quest';

interface PageCache {
    data: QuestContent[];
    lastUpdate: number;
}

interface CardsCache {
    data: Quest[];
    lastUpdate: number;
}

type PagesCache = Record<string, PageCache[]>;

export {
    PagesCache,
    CardsCache
};
