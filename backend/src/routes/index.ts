// 3-rd party libs
import { Router } from 'express';

// controllers
import getQuesCards from '../controllers/api/questCards';
import getQuest from '../controllers/api/getQuest';

const router = Router();

router.get('/cards', getQuesCards);
router.get('/quest', getQuest);

export default router;
