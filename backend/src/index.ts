// 3-rd party libs
import express from 'express';
import config from 'config';
import morgan from 'morgan';
// @ts-ignore
import cors from 'cors';

// utils
import * as path from 'path';
import logger from './utils/logger';

// routes
import routes from './routes';

// services
import initDataService from './dataService';

initDataService();

const app = express();
const assets = path.join(__dirname, 'assets');

if (config.get('debug')) {
    app.use(morgan('dev'));
}

const allowedOrigins = config.get('allowedOrigins');

// @ts-ignore
app.use(cors({
    // @ts-ignore
    origin(origin, callback) {
        // allow requests with no origin
        // (like mobile apps or curl requests)
        if (!origin) return callback(null, true);
        // @ts-ignore
        if (allowedOrigins.indexOf(origin) === -1) {
            const msg = 'The CORS policy for this site does not '
                + 'allow access from the specified Origin.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    }
}));

app.use(express.static(assets));
app.use('/api', routes);

const port = config.get('port');

app.listen(port, () => {
    logger.info(`Server started on ${port}`);
    logger.debug(`Open http://localhost:${port}/`);
});
