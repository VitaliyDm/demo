// interfaces
import { Request, Response } from 'express';

// services
import { getQuestCards } from '../../dataService';

const queryParams = {
    filterTag: 'filterTag',
    limit: 'limit',
    offset: 'offset'
};

const questCards = async (req: Request, res: Response) => {
    const { limit, offset = 0 } = req.query;
    let data;

    if (queryParams.filterTag in req.query) {
        const { filterTag } = req.query;

        const filteredQuests = (await getQuestCards())
            .filter(item => item.tags.map(tag => tag.link).includes(filterTag));

        data = {
            filterTag: (filteredQuests[0] || {}).tags.find(item => item.link === filterTag)?.title,
            cards: filteredQuests
        };
    } else {
        data = { cards: await getQuestCards() };
    }

    res.json({ ...data, cards: data.cards.slice(offset, offset + (limit || data.cards.length)) });
};

export default questCards;
