// interfaces
import { Request, Response } from 'express';

// constants
import errors from '../../constants/errors';

// utils
import logger from '../../utils/logger';
import { getQuestPages } from '../../dataService';

const getQuest = async (req: Request, res: Response) => {
    let content;
    const currentPage = parseInt(req.query.pageIndex, 10);

    if (isNaN(currentPage)) {
        logger.error(`failed get quest page by path: ${req.path}`);
        res.status(404)
            .json(errors.notFound);
        return;
    }

    try {
        content = await getQuestPages(req.query.questName, currentPage - 1);
    } catch (e) {
        logger.error(`failed get pages data: ${e}`);
        res.status(500).json(errors.internalError);
        return;
    }

    res.json(content);
};

export default getQuest;
