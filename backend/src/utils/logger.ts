// 3-rd party libs
import * as winston from 'winston';

const levels = {
    error: 0,
    warn: 1,
    info: 2,
    verbose: 3,
    debug: 4,
    silly: 5
};

const transports = [
    new winston.transports.Console(),
];

export default winston.createLogger({
    levels,
    transports
});
