export default function () {
    return Math.trunc(Date.now() / 1000);
}
